@extends('layout')
@section('content')
<h2>Product CRUD</h2>
<h2>@if ($id==-1)
      เพิ่ม
    @else
      แก้ไข
    @endif
สินค้า</h2>
@if ($id == -1)
<form method="post" action="/product/create">
  {{ csrf_field() }}
  <font>ชื่อสินค้า</font> <input type="text" name="name"><br><br>
  <font>ราคาต่อหน่วย</font> <input type="text" name="price"><br><br>
  <input type="submit" value="บันทึก">
</form>
@else
<form method="post" action="/product/edit">
  {{ csrf_field() }}
  <font>ชื่อสินค้า</font> <input type="text" name="name" value="{{$pro_id->name}}"><br><br>
  <font>ราคาต่อหน่วย</font> <input type="text" name="price" value="{{$pro_id->price}}"><br><br>
  <input type="hidden" name="id" value="{{$id}}">
  <input type="submit" value="บันทึก">
</form>
@endif
<hr>
<form  method="post" action="/product/search">
  {{ csrf_field() }}
  <input type="submit" value="ค้นหา"> <input type='text' name='keyword' size=60>
</form>
<table>
  @foreach ($pros as $pro)
  <tr>
    <td>{{$pro->name}}</td>
    <td>{{$pro->price}}</td>
    <td>
      <img src="/images/edit.png" width='40' height='40' onclick="edit({{$pro->id}})">
    </td>
  </tr>
  @endforeach
</table>


@stop

<script>
  function deleteIngre(id) {
    window.location = "/ingredient/delete/"+id;
  }

  function edit(id) {
    window.location = "/product/"+id;
  }

</script>
