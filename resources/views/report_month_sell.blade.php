@extends('layout')
@section('content')
<h2>รายงานยอดขายรายเดือน</h2><br>
<form method="get" action="">
  {{ csrf_field() }}
  <select id="month">
    @foreach ($months_list as $ml)
      <option value="{{$ml['i']}}"
        @if ($sel_month == $ml['i'])
          selected
        @endif
      >{{$ml['thai']}}</option>
    @endforeach
  </select>
  <select id='year'>
    @foreach ($years as $yr)
      <option value="{{$yr}}">{{$yr}}</option>
    @endforeach
  </select>
  <input type="button" value="เลือก" onclick="show()">
</form>
<script>
  function show() {
    mon = $('#month').val()
    year = $('#year').val()
    window.location = '/report/amountMonthSell/'+year+'/'+mon;
  }
</script>
<br>

<table border=1>
  <th>วันที่</th>
  <th>ชื่อสินค้า</th>
  <th>จำนวนขาย</th>
  <th>ราคาต่อหน่วย</th>
  <th>ราคารวม</th>
  <th>ชื่อลูกค้า</th>
  @foreach($sells as $sell)
    <tr>
      <td>{{$sell->date}}</td>
      <td>{{$sell->product_name}}</td>
      <td align='center'>{{$sell->amount}}</td>
      <td align='center'>{{$sell->unit_price}}</td>
      <td align='center'>{{$sell->total_price}}</td>
      <td align='center'>{{$sell->customer_name}}</td>
    </tr>
  @endforeach
</table><br>
<b>ราคารวมทั้งเดือน {{$total_sold}}</b>
@stop
