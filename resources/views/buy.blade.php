@extends('layout')
@section('content')
<h2>การซื้อสินค้าเข้ามา</h2>
@if ($id == -1)
<h3>เพิ่มการซื้อ</h3>
<form method="post" action="/buy/create">
  {{ csrf_field() }}
  <table>
    <tr>
      <td>วันที่</td>
      <td><input type="date" name='date' ></td>
    </tr>
    <tr>
      <td>ชื่อสินค้า</td>
      <td>
        <select style="font-size:16px;" name='product_id'>
          @foreach ($pros as $pro)
          <option value="{{$pro->id}}">{{$pro->name}}</option>
          @endforeach
        </select>
      </td>
    </tr>
    <tr>
      <td>จำนวน</td>
      <td><input type="text" name="amount"></td>
    </tr>
    <tr>
      <td>ราคา</td>
      <td><input type="text" name="price"></td>
    </tr>
  </table>
  <input type="submit" value="บันทึก">
</form>
@else
<h3>แก้ไขการซื้อ</h3>
<form method="post" action="/buy/edit">
  {{ csrf_field() }}
  <table>
    <tr>
      <td>วันที่</td>
      <td><input type="date" name='date' value='{{$buy_id->plain_date}}'></td>
    </tr>
    <tr>
      <td>ชื่อสินค้า</td>
      <td>
        <select style="font-size:16px;" name='product_id'>
          @foreach ($pros as $pro)
          <option value="{{$pro->id}}"
            @if ($pro->id == $buy_id->product_id)
              selected
            @endif
            >{{$pro->name}}</option>
          @endforeach
        </select>
      </td>
    </tr>
    <tr>
      <td>จำนวน</td>
      <td><input type="text" name="amount" value="{{$buy_id->amount}}"></td>
    </tr>
    <tr>
      <td>ราคาต่อหน่วย</td>
      <td><input type="text" name="unit_price" value="{{$buy_id->unit_price}}"></td>
    </tr>
  </table><br>
  <input type='hidden' name="buy_id" value='{{$id}}'>
  <input type="submit" value="บันทึก">
</form>
@endif

<hr>
<table border=1>
  <thead>
    <tr>
      <th>วันที่</th>
      <th>รายการ</th>
      <th>จำนวน</th>
      <th> ราคา </th>
    </tr>
  </thead>
  @foreach ($buys as $buy)
  <tr>
    <td>{{$buy->thai_dt}}</td>
    <td>{{$buy->product_name}}</td>
    <td align='center'>{{$buy->amount}}</td>
    <td>{{number_format($buy->unit_price, 2, '.', ',')}}</td>
    <td >
      <img src="/images/edit.png" width='40' height='40' onclick="window.location = '/buy/'+'{{$buy->id}}'">
    </td>
    <td >
      <img src="/images/x.png" width='30' height='30' onclick="window.location = '/buy/delete/'+'{{$buy->id}}'">
    </td>
  </tr>
  @endforeach
</table>

@stop
