<html>
<head>
  <title>BuchaDhamVanich POS</title>
  <meta charset="UTF-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{ url('/css/menu.css') }}" />

</head>
<body>
  <div id='layout'>

    <div class="dropdown">
      <button class="dropbtn">ซื้อ</button>
      <div class="dropdown-content">
        <a href="/buy/-1">ซื้อ</a>
      </div>
    </div>
    <div class="dropdown">
      <button class="dropbtn">ขาย</button>
      <div class="dropdown-content">
        <a href="/sell/-1">ขาย</a>
      </div>
    </div>
    <div class="dropdown">
      <button class="dropbtn">สินค้า</button>
      <div class="dropdown-content">
        <a href="/product/-1">สินค้า</a>
      </div>
    </div>
    <div class="dropdown">
      <button class="dropbtn">ลูกค้า</button>
      <div class="dropdown-content">
        <a href="/customer/-1">ลูกค้า</a>
      </div>
    </div>
    <div class="dropdown">
      <button class="dropbtn">รายงาน</button>
      <div class="dropdown-content">
        <a href="/report/amountLeft">สินค้าคงเหลือ</a>
        <a href="/report/amountMonthSell/-1/-1">การขายรายเดือน</a>
      </div>
    </div>
    <input id='log' type=button value='Logout' style="height:50px;width:70px" onclick='logout()'>



    @yield('content')

  </div>

  <script>
    function logout() {
      window.location = '/login/logout';
    }

    var $win = $(window);
    var $lay = $('#layout');
    var baseSize = {
      w: 600,
      h: 600
    }
    function updateScale() {
      var ww = $win.width();
      var wh = $win.height();
      var newScale = 1;
      var weight = 0.62;
      // compare ratios
      if(ww/wh < baseSize.w/baseSize.h) { // tall ratio
          newScale = ww / baseSize.w * weight;
          //newScale = 1.3;
      } else { // wide ratio
          newScale = wh / baseSize.h * weight;
          //newScale = 1.3;
      }

      $lay.css('transform', 'scale(' + newScale + ',' +  newScale + ')');


      //$('#xxx').html(newScale);
  }

  //$(window).resize(updateScale);

  $(window).on('load', function(){
    //updateScale()

    $.ajax({url: "/login/check", success: function(res){
      if (res == 1) {
        $('#log').show();
      } else {
        $('#log').hide();
      }
    }});

  });

  </script>
</body>
</html>
