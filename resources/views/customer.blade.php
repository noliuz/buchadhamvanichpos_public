@extends('layout')
@section('content')
<h2>ลูกค้า</h2>

<h2>@if ($id==-1)
      เพิ่ม
    @else
      แก้ไข
    @endif
ลูกค้า</h2>
@if ($id == -1)
<form method="post" action="/customer/create">
  {{ csrf_field() }}
  <table>
    <tr>
      <td>
        <font>ชื่อ</font>
      </td>
      <td>
        <input type="text" size=50 name="name">
      </td>
    </tr>
    <tr>
      <td>
        <font>ที่อยู่</font>
      </td>
      <td>
        <textarea rows="3" cols="50" name="address"></textarea>
      </td>
    </tr>
    <tr>
      <td>
        <font>เบอร์โทร</font>
      </td>
      <td>
        <input type="text" name="tel">
      </td>
    </tr>
    <tr>
      <td>
        <font>ไลน์</font>
      </td>
      <td>
        <input type="text" name="line" size=30>
      </td>
    </tr>
    <tr>
      <td>
        <font>เฟสบุ๊ค</font>
      </td>
      <td>
        <input type="text" name="fb" size=50>
      </td>
    </tr>
    <tr>
      <td>
        <font>อีเมล์</font>
      </td>
      <td>
        <input type="text" name="email" size=50>
      </td>
    </tr>
    <tr>
      <td>
        <br>
        <input type="submit" value="บันทึก">
      </td>
    </tr>
</table>
</form>
@else
<form method="post" action="/customer/edit">
  {{ csrf_field() }}
  <table>
    <tr>
      <td><font>ชื่อ</font></td>
      <td><input type="text" size=50 name="name" value="{{$cust_id->name}}"></td>
    </tr>
    <tr>
      <td><font>ที่อยู่</font></td>
      <td><textarea rows="3" cols="50" name="address">{{$cust_id->address}}</textarea>
    </tr>
    <tr>
      <td><font>เบอร์โทร</font></td>
      <td><input type="text" size=15 name="tel" value="{{$cust_id->tel}}"></td>
    </tr>
    <tr>
      <td><font>ไลน์</font></td>
      <td><input type="text" size=30 name="line" value="{{$cust_id->line}}"></td>
    </tr>
    <tr>
      <td><font>เฟสบุ๊ค</font></td>
      <td><input type="text" size=30 name="fb" value="{{$cust_id->fb}}"></td>
    </tr>
    <tr>
      <td><font>อีเมล์</font></td>
      <td><input type="text" size=50 name="email" value="{{$cust_id->email}}"></td>
    </tr>
  </table><br>
  <input type="hidden" name="id" value="{{$id}}">
  <input type="submit" value="บันทึก">
</form>
@endif
<hr>
<form  method="post" action="/customer/search">
  {{ csrf_field() }}
  <input type="submit" value="ค้นหา"> <input type='text' name='keyword' size=60>
</form>

<table>
  @foreach ($custs as $cust)
  <tr>
    <td><b>ชื่อ</b> {{$cust->name}}</td>
    <td><b>ที่อยู่</b> {{$cust->address}}</td>
  </tr>
  <tr>
    <td><b>เบอร์โทร</b> {{$cust->tel}}</td>
    <td><b>ไลน์</b> {{$cust->line}}</td>
  </tr>
  <tr>
    <td><b>เฟสบุ๊ค</b> {{$cust->fb}}</td>
    <td><b>อีเมล์</b> {{$cust->email}}</td>
    <td>
      <img src="/images/edit.png" width='40' height='40' onclick="edit({{$cust->id}})">
    </td>
  </tr>
  <tr><td><hr></td></tr>
  @endforeach
</table>


@stop

<script>
  function edit(id) {
    window.location = "/customer/"+id;
  }

</script>
