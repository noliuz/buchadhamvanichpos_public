@extends('layout')
@section('content')
<h2>รายงานสินค้าคงเหลือ</h2>
<br>
  <table border=1>
    <th>ชื่อสินค้า</th>
    <th>จำนวนคงเหลือ</th>
    @foreach($pros as $pro)
      <tr>
        <td>{{$pro->name}}</td>
        <td align='center'>{{$pro->left_amount}}</td>
      </tr>
    @endforeach

  </table>

@stop
