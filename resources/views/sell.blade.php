@extends('layout')

@section('content')
<script>
  $(window).on('load', function(){
    changeAmount();

  });

  function changeAmount() {
    pro_id = $('#select_product_name').val();
    $.ajax('/func/getProductLeftAmountFromProductID/'+pro_id,{
        success: function (data, status, xhr) {
                    $('#left_amount').html('เหลือ '+data+' ชิ้น');
                 }
    });

  }
</script>

<h2>การขาย</h2>
@if ($id == -1)
<h3>เพิ่มการขาย</h3>
<form method="post" action="/sell/create">
  {{ csrf_field() }}
  <table>
    <tr>
      <td>วันที่</td>
      <td><input type="date" name='date' ></td>
    </tr>
    <tr>
      <td>ชื่อสินค้า</td>
      <td>
        <select id="select_product_name" style="font-size:16px;" name='product_id' onchange="changeAmount();">
          @foreach ($pros as $pro)
          <option value="{{$pro->id}}">{{$pro->name}}</option>
          @endforeach
        </select>
      </td>
    </tr>
    <tr>
      <td>จำนวน</td>
      <td><input type="text" name="amount"> <span id='left_amount'> ชิ้น</div></td>
    </tr>
    <tr>
      <td>ราคาขายต่อหน่วย</td>
      <td><input type="text" name="unit_price"></td>
    </tr>
    <tr>
      <td>ชื่อลูกค้า</td>
      <td>
        <input type="text" list="customer_names" name="customer_name" />
        <datalist id="customer_names">
          @foreach ($customer_names as $cust_name)
            <option value="{{$cust_name}}">
          @endforeach
        </datalist>
      </td>
    </tr>
  </table>
  <input type="submit" value="บันทึก">
</form>
@else
<h3>แก้ไขการขาย</h3>
<form method="post" action="/sell/edit">
  {{ csrf_field() }}
  <table>
    <tr>
      <td>วันที่</td>
      <td><input type="date" name='date' value='{{$sell_id->plain_date}}'></td>
    </tr>
    <tr>
      <td>ชื่อสินค้า</td>
      <td>
        <select style="font-size:16px;" name='product_id'>
          @foreach ($pros as $pro)
          <option value="{{$pro->id}}"
            @if ($pro->id == $sell_id->product_id)
              selected
            @endif
          >{{$pro->name}}
          </option>
          @endforeach
        </select>
      </td>
    </tr>
    <tr>
      <td>จำนวน</td>
      <td><input type="text" name="amount" value='{{$sell_id->amount}}'></td>
    </tr>
    <tr>
      <td>ราคาขายต่อหน่วย</td>
      <td><input type="text" name="unit_price" value='{{$sell_id->unit_price}}'></td>
    </tr>
    <tr>
      <td>ชื่อลูกค้า</td>
      <td>
        <input type="text" list="customer_names" name="customer_name" /value='{{$sell_id->customer_name}}'>
        <datalist id="customer_names">
          @foreach ($customer_names as $cust_name)
            <option value="{{$cust_name}}">
          @endforeach
        </datalist>
      </td>
    </tr>
  </table>
  <input type='hidden' name="sell_id" value='{{$id}}'>
  <input type="submit" value="บันทึก">
</form>
@endif
<hr>
<form  method="post" action="/sell/search">
  {{ csrf_field() }}
  <input type="submit" value="ค้นหา"> <input type='text' name='keyword' size=60>
  <input type='hidden' value={{$id}} >
</form>


<table border=1>
    <th>วันที่</th>
    <th>ราคาสินค้า</th>
    <th>จำนวน</th>
    <th>ราคาขายต่อหน่วย</th>
    <th>ชื่อลูกค้า</th>
  @foreach($sells as $sell)
    <tr>
      <td>{{$sell->th_date}}</td>
      <td>{{$sell->product_name}}</td>
      <td>{{$sell->amount}}</td>
      <td>{{$sell->unit_price}}</td>
      <td>{{$sell->customer_name}}</td>
      <td >
        <img src="/images/edit.png" width='40' height='40'
        onclick="window.location='/sell/'+'{{$sell->id}}'">
      </td>
      <td>
        <img src="/images/x.png" width='30' height='30' onclick="window.location='/sell/delete/'+'{{$sell->id}}'">
      </td>
    </tr>
  @endforeach
</table>
@stop
