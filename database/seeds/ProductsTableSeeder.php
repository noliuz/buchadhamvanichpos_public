<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('products')->insert([
          'name' => 'Ice',
          'price' => 50

      ]);
      DB::table('products')->insert([
          'name' => 'Banana',
          'price' => 20

      ]);
      DB::table('products')->insert([
          'name' => 'Pork',
          'price' => 150

      ]);
    }
}
