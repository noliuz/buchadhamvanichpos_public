<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('customers')->insert([
          'name' => 'Nol',
          'address' => '227/46 Chaiyaphum Thailand',
          'tel' => '0831103806',
          'line' => '083113806',
          'fb' => 'Trin Tarstrirutt',
          'email' => 'noliuz@gmail.com'
      ]);
      DB::table('customers')->insert([
          'name' => 'Naruemol',
          'address' => 'Bangkok Thailand',
          'tel' => '0816098441',
          'line' => 'maw008',
          'fb' => 'Naruemol Pathomvanit',
          'email' => 'neruemol1100@gmail.com'      
      ]);
    }
}
