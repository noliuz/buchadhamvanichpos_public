<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'throttle:20,20'], function () {
  Route::get('/','FuncController@loginForm');
  Route::post('/login/login','FuncController@login');
});


Route::group(['middleware' => ['auth']], function () {

  Route::get('/login/logout', function () {
    Auth::logout();
    return redirect('/');
  });
  Route::get('/login/check', function () {
    if (Auth::check()) {
      return 1;
    } else {
      return 0;
    }
  });

  Route::get('/report/amountLeft','ReportController@amountLeft');
  Route::get('/report/amountMonthSell/{year}/{month}','ReportController@amountMonthSell');


  Route::get('/func/getProductLeftAmountFromProductID/{id}','FuncController@getProductLeftAmountFromProductID');

  Route::get('/sell/{id}','SellController@index');
  Route::get('/sell/delete/{id}','SellController@delete');
  Route::post('/sell/create','SellController@create');
  Route::post('/sell/edit','SellController@edit');
  Route::post('/sell/search','SellController@search');

  Route::get('/buy/{id}','BuyController@index');
  Route::post('/buy/create','BuyController@create');
  Route::post('/buy/edit','BuyController@edit');
  Route::get('/buy/delete/{id}','BuyController@delete');


  Route::post('/product/edit','ProductController@edit');
  Route::post('/product/create','ProductController@create');
  Route::get('/product/{id}','ProductController@index');
  Route::post('/product/search','ProductController@search');

  Route::get('/customer/{id}','CustomerController@index');
  Route::post('/customer/search','CustomerController@search');
  Route::post('/customer/create','CustomerController@create');
  Route::post('/customer/edit','CustomerController@edit');

});
