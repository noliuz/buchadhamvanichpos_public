<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buy;
use App\Product;

class BuyController extends Controller
{
  public function create(Request $req) {
    if ( empty($req->product_id) || empty($req->price) ||
        empty($req->amount) || empty($req->date)
       )
      return 'Error, must fill all field';
    $buy = new Buy;
    $buy->product_id = $req->product_id;
    $buy->unit_price = $req->price;
    $buy->amount = $req->amount;
    $buy->created_at = $req->date;
    $buy->save();

    return redirect('/buy/-1');
  }

  public function edit(Request $req) {
    $buy = Buy::find($req->buy_id);
    $buy->created_at = $req->date;
    $buy->product_id = $req->product_id;
    $buy->amount = $req->amount;
    $buy->unit_price = $req->unit_price;
    $buy->save();

    return redirect('/buy/-1');
  }

  public function delete($id) {
    $buy = Buy::find($id);
    $buy->delete();

    return redirect('/buy/-1');
  }

  public function index($id) {
    $buys = Buy::orderBy('created_at','desc')->get();
    foreach ($buys as $buy) {
      $buy->product_name = $buy->products->name;
      $thai_year = date('Y',strtotime($buy->created_at))+543;
      $buy->thai_dt = date('d/m/',strtotime($buy->created_at));
      $buy->thai_dt .= $thai_year;
    }

    //product name and id
    $pros = Product::orderBy('name')->get();

    if ($id == -1) {
      return view('buy',['buys'=>$buys,'id'=>$id,'pros'=>$pros]);
    } else {
      $buy_id = Buy::find($id);
      $buy_id->plain_date = date('Y-m-d',strtotime($buy_id->created_at));
      return view('buy',['buys'=>$buys,'id'=>$id,'buy_id'=>$buy_id,'pros'=>$pros]);
    }
  }
}
