<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sell;
use App\Buy;
use App\Product;
use App\Customer;
use Illuminate\Support\Facades\DB;


class SellController extends Controller
{
  public function search(Request $req) {
    $kw = $req->keyword;

    $sells = DB::table('sells')
        ->leftJoin('customers','customer_id','=','customers.id')
        ->leftJoin('products','product_id','=','products.id')
        ->where('customers.name','LIKE',"%$kw%")
        ->orWhere('products.name','LIKE',"%$kw%")
        ->orWhere('sells.unit_price','=',"$kw")
        ->orWhere('sells.amount','=',"$kw")
        ->orWhere('sells.created_at','LIKE',"%$kw%")
        ->select('products.name as product_name',
                 'customers.name as customer_name',
                 'sells.created_at as created_at',
                 'sells.amount as amount',
                 'sells.unit_price as unit_price',
                 'sells.id as id'
               )
        ->get();
    //return $sells;
    foreach ($sells as $sell) {
      $sell->th_date = date("d/m/Y",strtotime($sell->created_at));
    }

    $pros = Product::orderBy('name')->get();
    $custs = Customer::orderBy('name')->get();
    $customer_names = [];
    foreach($custs as $cust) {
      array_push($customer_names,$cust->name);
    }

    return view('sell',['sells'=>$sells,'id'=> -1,'pros'=>$pros,'customer_names'=>$customer_names]);
  }

  public function edit(Request $req) {
    $sell = Sell::find($req->sell_id);
    $sell->created_at = $req->date;
    $sell->product_id = $req->product_id;
    $sell->amount = $req->amount;
    $sell->unit_price = $req->unit_price;

    $cust = Customer::where('name','LIKE',trim($req->customer_name))->first();
    $sell->customer_id = $cust->id;

    $sell->save();

    return redirect('/sell/-1');
  }

  public function create(Request $req) {
    $sell = new Sell;
    $sell->created_at = $req->date;
    $sell->product_id = $req->product_id;
    $sell->amount = $req->amount;
    $sell->unit_price = $req->unit_price;

    //find customer id
    $cust = Customer::where('name','LIKE',trim($req->customer_name))->first();
    $sell->customer_id = $cust->id;
    $sell->save();


    return redirect('/sell/-1');
  }

  public function index($id) {
    $sells = Sell::orderBy('created_at','desc')->get();
    foreach ($sells as $sell) {
      $sell->product_name = $sell->products->name;
      $sell->customer_name = $sell->customers->name;
      $sell->th_date = date("d/m/Y",strtotime($sell->created_at));
    }

    $pros = Product::orderBy('name')->get();
    $custs = Customer::orderBy('name')->get();
    $customer_names = [];
    foreach($custs as $cust) {
      array_push($customer_names,$cust->name);
    }    

    if ($id == -1) {
      return view('sell',['sells'=>$sells,'pros'=>$pros,'customer_names'=>$customer_names,'id'=>$id]);
    } else {
      $sell_id = Sell::find($id);
      $sell_id->plain_date = date('Y-m-d',strtotime($sell_id->created_at));
      $sell_id->product_name = $sell_id->products->name;
      $sell_id->customer_name = $sell_id->customers->name;
      return  view('sell',['sells'=>$sells,'pros'=>$pros,'customer_names'=>$customer_names,'id'=>$id,'sell_id'=>$sell_id]);
    }
  }

  public function delete($id) {
    $sell = Sell::find($id);
    $sell->delete();

    return redirect('/sell/-1');
  }
}
