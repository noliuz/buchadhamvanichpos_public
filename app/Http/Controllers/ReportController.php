<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buy;
use App\Sell;
use App\Product;
use Illuminate\Support\Facades\DB;


class ReportController extends Controller
{
    public function amountLeft() {
      $total_buys = Buy::all();
      $total_sells = Sell::all();
      $pros = Product::all();
      foreach ($pros as $product) {

        $sell_amount = 0;
        foreach ($total_sells as $t_sell) {
          if ($t_sell->product_id == $product->id) {
            $sell_amount += $t_sell->amount;
          }
        }

        $buy_amount = 0;
        foreach ($total_buys as $t_buy) {
          if ($t_buy->product_id == $product->id) {
            $buy_amount += $t_buy->amount;
          }
        }

        //$product->left_amount = $buy_amount - $sell_amount;
        $left_amount = $buy_amount - $sell_amount;
        $product->left_amount = $left_amount;
      }

      return view('report_amount_left',['pros'=>$pros]);
    }

    public function amountMonthSell($year,$month) {
      if ($year == -1)
        $year = date('Y')+543;
      if ($month == -1)
        $month = date('m');

      $days = cal_days_in_month(CAL_GREGORIAN, $month, $year); // 31
      $year -= 543;
      $from_date = "$year-$month-01";
      $to_date = "$year-$month-$days";
      $sells = Sell::where('created_at','>=',$from_date)
                    ->where('created_at','<=',$to_date)
                    ->orderBy('created_at')
                    ->get();
      $total_sold = 0;
      foreach($sells as $sell) {
        $sell->product_name = $sell->products->name;
        $sell->customer_name = $sell->customers->name;
        $sell->total_price = $sell->amount*$sell->unit_price;
        $total_sold += $sell->total_price;

        $yr = date('Y',strtotime($sell->created_at));
        $yr += 543;
        $date = date('d/m/',strtotime($sell->created_at));
        $date .= $yr;
        $sell->date = $date;
      }

      $res = Sell::selectRaw('year(created_at) year, count(*) data')
                ->groupBy('year')
                ->orderBy('year', 'desc')
                ->get();
      $years = [];
      foreach ($res as $r) {
        array_push($years,$r->year+543);
      }

      $sel_month = $month;
      $months_list = [];
      $thaimonth=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");

      for ($i=1;$i<=12;$i++) {
        $m = [];
        $m['i'] = $i;
        $m['thai'] = $thaimonth[$i-1];
        array_push($months_list,$m);
      }

      //return $months_list;

      return view('report_month_sell',['sells'=>$sells,'total_sold'=>$total_sold,'years'=>$years,'months_list'=>$months_list,'sel_month'=>$sel_month]);

    }
}
