<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buy;
use App\Sell;
use App\Product;
use Auth;

class FuncController extends Controller
{
  public function login(Request $req) {
    $email = $req->email;
    $password = $req->password;

    if (Auth::attempt(array('email' => $email, 'password' => $password), true)) {
      return redirect('/sell/-1');
    } else {
      return 'error login';
    }    
  }

  public function loginForm() {
    return view('login');
  }

  public function getProductLeftAmountFromProductID($id) {
    //find left on every products
    $total_buys = Buy::all();
    $total_sells = Sell::all();
    $pros = Product::all();
    foreach ($pros as $product) {
      if ($product->id != $id)
        continue;

      $sell_amount = 0;
      foreach ($total_sells as $t_sell) {
        if ($t_sell->product_id == $product->id) {
          $sell_amount += $t_sell->amount;
        }
      }

      $buy_amount = 0;
      foreach ($total_buys as $t_buy) {
        if ($t_buy->product_id == $product->id) {
          $buy_amount += $t_buy->amount;
        }
      }

      //$product->left_amount = $buy_amount - $sell_amount;
      $left_amount = $buy_amount - $sell_amount;

      return $left_amount;
    }

  }
}
