<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function index($id) {
      $custs = Customer::all();
      if ($id == -1) {
        return view('customer',['custs'=>$custs,'id'=>$id]);
      } else {
        $cust_id = Customer::find($id);
        return view('customer',['custs'=>$custs,'id'=>$id,'cust_id'=>$cust_id]);
      }
    }

    public function create(Request $req) {
      if ( empty($req->name) || empty($req->address) )
        return 'Error, must fill name and address';

      $cust = new Customer;
      $cust->name = $req->name;
      $cust->address = $req->address;
      if (isset($req->tel))
        $cust->tel = $req->tel;
      if (isset($req->line))
        $cust->line = $req->line;
      if (isset($req->fb))
        $cust->fb = $req->fb;
      if (isset($req->email))
        $cust->email = $req->email;

      $cust->save();

      return redirect('/customer/-1');
    }

    public function edit(Request $req) {
      $cust = Customer::find($req->id);
      $cust->name = $req->name;
      $cust->address = $req->address;
      $cust->tel = $req->tel;
      $cust->line = $req->line;
      $cust->fb = $req->fb;
      $cust->email = $req->email;
      $cust->save();

      return redirect('/customer/-1');
    }

    public function search(Request $req) {
      $kw = $req->keyword;
      $custs = Customer::where('name', 'LIKE', "%$kw%")
            ->orWhere('address', 'LIKE', "%$kw%")
            ->orWhere('tel', 'LIKE', "%$kw%")
            ->orWhere('line', 'LIKE', "%$kw%")
            ->orWhere('fb', 'LIKE', "%$kw%")
            ->orWhere('email', 'LIKE', "%$kw%")
            ->get();
      return view('customer',['custs'=>$custs,'id'=>-1]);
    }
}
