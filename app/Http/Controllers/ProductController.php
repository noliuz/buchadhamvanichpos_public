<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function create(Request $req) {
      if (empty($req->name) || empty($req->price))
        return 'Error, must fill all field';
      $product = new Product;
      $product->name = $req->name;
      $product->price = $req->price;
      $product->save();

      return redirect('/product/-1');
    }

    public function index($id) {
      $pros = Product::all();
      if ($id == -1) {
        return view('product',['pros'=>$pros,'id'=>$id]);
      } else {
        $pro_id = Product::find($id);
        return view('product',['pros'=>$pros,'id'=>$id,'pro_id'=>$pro_id]);
      }
    }

    public function edit(Request $req) {
      $pro = Product::find($req->id);
      $pro->name = $req->name;
      $pro->price = $req->price;
      $pro->save();

      return redirect('/product/-1');
    }

    public function search(Request $req) {
      $kw = $req->keyword;
      $pros = Product::where('name', 'LIKE', "%$kw%")
            ->orWhere('price', 'LIKE', "%$kw%")
            ->get();
      return view('product',['pros'=>$pros,'id'=>-1]);
    }
}
