<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  public function sells()
  {
      return $this->hasMany('App\Sell');
  }
}
