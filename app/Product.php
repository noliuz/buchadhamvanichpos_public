<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  public function buys()
  {
      return $this->hasMany('App\Buy');
  }

  public function sells()
  {
      return $this->hasMany('App\Sell');
  }
}
